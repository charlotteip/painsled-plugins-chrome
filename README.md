# painsled-plugins
Data conversion and transfer plugins for the [PainSled Indoor Rowing Dashboard](http://painsled.com/ "PainSled.com").

Docs are TBD, for now, play with the example code. Keep in mind that the JS workout and stroke data structures will likely change while the PainSled app is in beta.

## Using Export Plugins

Open the logs panel, select some workouts to export, then select the "plugin" button, then pick a plugin. You'll be prompted to save the file after the workouts have been converted. After using a plugin it will be added to the export dialog button bar for easy future access. If an error occurs during conversion, a window will pop open that can be used to debug the plugin (see below).

## Developing Export Plugins

Model your plugin off of one of the example plugins. Your plugin will run in a separate Webview from the main PainSled app window. Debugging is accomplished by right-clicking the Webview after an error occurs to open the Chrome Developer Tools. You may then set breakpoints and rerun the plugin to debug it.

You can also force the Webview plugin window open BEFORE running your plugin if you need to set breakpoints with the Chrome developer tools. To do that, select the plugin button but hit cancel instead of selecting a new plugin. After the file selection window closes the Webview window will pop open and you can set breakpoints. Then rerun your plugin to debug.

### Workout Data Format

The current plugin has a single API, convertBatch(), that converts a list of workouts into a chunk of text. The workout data is essentially the internal respresentation used by the PainSled app itself and is therefore subject to change, especially during the beta period. However, it generally tracks the raw values returned by the PM USB interface. The spec for that data can be found in the [Concept2 SDK](http://www.concept2.com/service/software/software-development-kit).

Although the current workout data structures are not (currently) documented, the basic organization is workouts[0..n].strokes[0..n].samples[0..n] with each structure having additional properties. Until workouts are documented, you may be able to reference the more detailed example plugins or set a breakpoint and browse the workouts data structures directly.

Each structure is also revisioned in case your plugin only works with specific versions of data. See [painsled2samples.js](https://github.com/charlotte-ip/painsled-plugins/blob/master/export/painsled2samples.js "painsled2samples.js") for an example.

## Exporting: Data Conversion OR Upload.

Export plugins can do more than simply convert data into new output formats. As first-class JS modules, they may also upload data to web services. If a plugin is created to only upload data, returning an empty fileExt value will signal PainSled to skip the file save step after export.

## Export UI

There is currently no plugin-definable UI so any information needed by your plugin will have to be hard-coded in by its users. This is expected to change at some point, in particular for data sync export plugins that may need to ask for user credentials before uploading data. 

## Including External JS Libraries

Although your plugin may inject remote JS libraries into the Webview, this will limit your plugin to online usage only. There is no mechanism for including JS libraties at present, so copying them into your plugin may be an appropriate stop-gap.
