/*
 This file is from the painsled-plugins repository, https://github.com/charlotte-ip/painsled-plugins
 Copyright (c) 2015, Charlotte Intellectual Properties, LLC, All rights reserved.

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 3.0 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library.
 */

// This plugin dumps stroke sample data as CSV text.
function PainsledPluginLibrary() {

    function convertBatch(workouts) {
        // Temporary hack for PainSled <= v0.8 which passes workouts in latest-first order.
        workouts.sort(function(a, b) {return a.startMs - b.startMs});

        var response = {content: false, error: false};
		var output = 'sample.sampleMs, sample.workoutTime, sample.workoutMeters, sample.monitorTime, sample.monitorMeters, sample.watts, sample.paceSecPer1k, sample.strokeState, stroke.workoutId, stroke.intervalNumber, stroke.strokeNumber, stroke.strokeType, workout.workoutType, workout.intervalType\n';

        try {
//		    output += JSON.stringify(workouts, null, 2);
            if (workouts.length <= 0) throw 'No workouts to export.';

            // Output workouts
            for (var i = 0; i < workouts.length; i++) {
//                output += 'workout # ' + i + '\n';
                var workout = workouts[i];
                
                // If workout not minimum revision, skip.
                if (workout.REVISION < 4) {
                    output += '#### Workout #' + i + ' revision level of ' + workout.REVISION + ' is below the minimum of 4. Skipping. ####\n';
                    continue;
                }

                // For each stroke
                for (var j = 0; j < workout.strokes.length; j++) {
                    var stroke = workout.strokes[j];

					// For each stroke sample
					for (var k = 0; k < stroke.samples.length; k++) {
						var sample = stroke.samples[k];

						// Output a sample
						var values = [sample.sampleMs, sample.workoutTime, sample.workoutMeters, sample.monitorTime, sample.monitorMeters, sample.watts, sample.paceSecPer1k, sample.strokeState, stroke.workoutId, stroke.intervalNumber, stroke.strokeNumber, stroke.strokeType, workout.workoutType, workout.intervalType];
						for (var fieldIdx = 0; fieldIdx < values.length; fieldIdx++) {
                            output += values[fieldIdx] + ', ';
						}
                        output += '\n';
					}
                }
            }
        } catch (err) {
            response.error = 'Conversion to samples failed with ' + err;
        }
        response.content = output;
        return response;
    }

    // Return public members
    return {
        mimeType: 'text/plain', 
        fileExt: 'csv', 
        shortName: 'samples',
        description: 'Convert PainSled stroke sample data to CSV format.',
        convertBatch: convertBatch
    };
}