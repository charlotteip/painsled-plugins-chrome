/*
 This file is from the painsled-plugins repository, https://github.com/charlotte-ip/painsled-plugins
 Copyright (c) 2015, Charlotte Intellectual Properties, LLC, All rights reserved.

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 3.0 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library.
 */

// This plugin demonstrates how to return a failure.
function PainsledPluginLibrary() {
    // Return public members
    return {
        mimeType: 'text/plain',
        fileExt: 'txt',
        shortName: 'text',
        description: 'Fail every time we are called.',
        convertBatch: function(workouts) { return {content: false, error: "Conversion failed, as expected."}; }
    };
}