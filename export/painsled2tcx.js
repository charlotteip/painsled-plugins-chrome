/*
 This file is from the painsled-plugins repository, https://github.com/charlotte-ip/painsled-plugins
 Copyright (c) 2015, Charlotte Intellectual Properties, LLC, All rights reserved.

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 3.0 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library.
 */

// This plugin dumps workout and stroke data as Garmin TCX XML text.
function PainsledPluginLibrary() {
    function pace2mps(secPer1k) { return secPer1k > 0 ? 1000 / secPer1k : 0; }

    // Converts painsled workouts to TCX XML format
    function convertBatch(workouts) {
        // Temporary hack for PainSled <= v0.8 which passes workouts in latest-first order.
        workouts.sort(function(a, b) {return a.startMs - b.startMs});

        var response = {content: false, error: false};

        try {
            if (workouts.length <= 0) throw 'No workouts to export.';

            // Utils
            function isoDate(ms) {
                var date = new Date(ms);
                var s = date.toISOString();
                return s;
            }

            var w0 = workouts[0];
            var app = w0.appManifest;

            // Track lap/split distance, resets to 0 after each lap/split.
            var isLapOpen = false;
            var lapMeters = 0;

            // Output TCX preamble
            var output =
                '<?xml version="1.0" encoding="UTF-8"?>\n' +
                '<TrainingCenterDatabase\n' +
                'xsi:schemaLocation="http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2 http://www.garmin.com/xmlschemas/TrainingCenterDatabasev2.xsd"\n' +
                'xmlns:ns5="http://www.garmin.com/xmlschemas/ActivityGoals/v1"\n' +
                'xmlns:ns3="http://www.garmin.com/xmlschemas/ActivityExtension/v2"\n' +
                'xmlns:ns2="http://www.garmin.com/xmlschemas/UserProfile/v2"\n' +
                'xmlns="http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2"\n' +
                'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ns4="http://www.garmin.com/xmlschemas/ProfileExtension/v1">\n' +
                '<Activities>\n' +
                '<Activity Sport="Biking">\n' +
                '  <Id>' + isoDate(w0.startMs) + '</Id>\n';

			// Starts a lap/split
			function openLap(workout, intervalIdx, intervalStartingStrokeIdx, isRestingLap) {
                var wo = workout;
                var startingStroke = wo.strokes[intervalStartingStrokeIdx];

                if (isLapOpen) closeLap();  // close previous lap
                isLapOpen = true;

                // Calc stats
                var lastStroke;
                var maxMetersPerSec = 0.0;
                var hrSamples = 0;
                var avgBpm = 0;
                var maxBpm = 0;
                var k;
                var lapTotalMeters = 0
                for (k = intervalStartingStrokeIdx; k < wo.strokes.length; k++) {
                    // Bail if lap number changed or resting state changed
                    var restingState = wo.strokes[k].watts == 0;
                    if ((wo.strokes[k].intervalNumber != intervalIdx) || (restingState != isRestingLap)) break;

                    // Save last stroke in interval for later use
                    lastStroke = wo.strokes[k];

                    maxMetersPerSec = Math.max(maxMetersPerSec, pace2mps(lastStroke.paceSecPer1k));
                    if (lastStroke.hrBpm > 0) {
                        hrSamples++;
                        maxBpm = Math.max(maxBpm, lastStroke.hrBpm);
                        avgBpm += lastStroke.hrBpm;
                    }
                    lapTotalMeters += lastStroke.strokeMeters;
                }
                avgBpm = Math.round(avgBpm / hrSamples);    // Average bpm over all stroke samples.

                // Lap/interval preamble
                output +=
                    '  <Lap StartTime="' + isoDate(startingStroke.driveStartMs) + '">\n' +
                    '    <TotalTimeSeconds>' +
                    ((lastStroke.driveStartMs - startingStroke.driveStartMs + lastStroke.strokeTimeMs) / 1000).toFixed(1) +
                    '</TotalTimeSeconds>\n' +
                    '    <DistanceMeters>' + lapTotalMeters.toFixed(1) + '</DistanceMeters>\n' +
                    '    <MaximumSpeed>' + maxMetersPerSec.toFixed(1) + '</MaximumSpeed>\n' +
                    '    <Calories>' + (lastStroke.calorieCounter - startingStroke.calorieCounter) + '</Calories>\n';

                if (hrSamples > 0) {
                    output +=
                        '    <AverageHeartRateBpm>\n' +
                        '      <Value>' + avgBpm + '</Value>\n' +
                        '    </AverageHeartRateBpm>\n' +
                        '    <MaximumHeartRateBpm>\n' +
                        '      <Value>' + maxBpm + '</Value>\n' +
                        '    </MaximumHeartRateBpm>\n';
                }

                output +=
                    '    <Intensity>' + (isRestingLap ? 'Resting' : 'Active') + '</Intensity>\n' +
                    '    <TriggerMethod>Manual</TriggerMethod>\n' +
                    '    <Track>\n';
			}
			
			// Closes a lap/split
            function closeLap() {
                if (!isLapOpen) return;
                output +=
                    '    </Track>\n' +
                    '  </Lap>\n';
                isLapOpen = false;
            }

            // Output workouts
            for (var i = 0; i < workouts.length; i++) {
                var wo = workouts[i];
                var curLap = wo.strokes[0].intervalNumber;   // track current intervals/laps.

                // If workout not minimum revision, skip.
                if (wo.REVISION < 3) {
                    output += '<!-- Workout #' + i + ' revision level of ' + wo.REVISION + ' is below the minimum of 3. Skipping. -->\n';
                    continue;
                }

                // Track stroke state resting vs. working based on the presence of wattage.
                var lastResting = wo.strokes[0].watts === 0;

                // Every workout starts a new lap.
                openLap(wo, curLap, 0, lastResting);

                // For each stroke
                for (var j = 0; j < wo.strokes.length; j++) {
                    var stroke = wo.strokes[j];
                    var curResting = stroke.watts === 0;

                    // Insert additional laps for each interval in a workout.
                    if ((stroke.intervalNumber != curLap) || (curResting != lastResting)) {
                        openLap(wo, stroke.intervalNumber, j, curResting);
                        curLap = stroke.intervalNumber;
                    }
                    lastResting = curResting;

                    // Per stroke data
                    lapMeters += stroke.strokeMeters;
                    output +=
                        '      <Trackpoint>\n' +
                        '        <Time>' + isoDate(stroke.driveStartMs + stroke.strokeTimeMs) + '</Time>\n' +
                        '        <DistanceMeters>' + lapMeters + '</DistanceMeters>\n';

                    if (stroke.hrBpm > 0) {
                        output +=
                            '        <HeartRateBpm>\n' +
                            '          <Value>' + stroke.hrBpm + '</Value>\n' +
                            '        </HeartRateBpm>\n';
                    }

                    output +=
                        '        <Cadence>' + stroke.strokesPerMin + '</Cadence>\n' +
                        '        <SensorState>Present</SensorState>\n' +
                        '        <Extensions>\n' +
                        '          <TPX xmlns="http://www.garmin.com/xmlschemas/ActivityExtension/v2">\n' +
                        '            <Watts>' + stroke.watts + '</Watts>\n' +
                        '          </TPX>\n' +
                        '        </Extensions>\n' +
                        '      </Trackpoint>\n';
                }
                closeLap();
            }

            // Output postamble
            var verRevs = app.version.split('.');

            // Close workout
            output +=
                '  <Creator xsi:type="Device_t">\n' +
                '    <Name>Concept2</Name>\n' +
                '    <UnitId>' + wo.ergSerial + '</UnitId>\n' +
                '    <ProductID>' + wo.ergModel.replace('Concept2 PM', '') + '</ProductID>\n' +
                    // TODO: get device info into workout structure.
                '    <Version>\n' +
                '      <VersionMajor>' + 1 + '</VersionMajor>\n' +
                '      <VersionMinor>' + 1 + '</VersionMinor>\n' +
                '      <BuildMajor>' + 1 + '</BuildMajor>\n' +
                '      <BuildMinor>' + 1 + '</BuildMinor>\n' +
                '    </Version>\n' +
                '  </Creator>\n' +
                '</Activity>\n' +
                '</Activities>\n' +
                '<Author xsi:type="Application_t">\n' +
                '  <Name>' + app.name + '</Name>\n' +
                '  <Build>\n' +
                '    <Version>\n' +
                '      <VersionMajor>' + verRevs[0] + '</VersionMajor>\n' +
                '      <VersionMinor>' + verRevs[1] + '</VersionMinor>\n' +
                '      <BuildMajor>0</BuildMajor>\n' +
                '      <BuildMinor>0</BuildMinor>\n' +
                '    </Version>\n' +
                '  </Build>\n' +
                '  <LangID>en</LangID>\n' +
                '  <PartNumber>' + app.short_name + '</PartNumber>\n' +
                '</Author>\n' +
                '</TrainingCenterDatabase>\n';

            // Optionally dump JSON for debug
//            output += '\n<!--\n' + JSON.stringify(workouts, null, 2) + '\n-->\n';

            response.content = output;
        } catch (err) {
            response.error = 'Conversion to TCX failed with ' + err;
        }
        return response;
    }

    // Return public members
    return {
        mimeType: 'application/vnd.garmin.tcx+xml',
        fileExt: 'tcx',
        shortName: 'tcx',
        description: 'Convert PainSled data to Garmin TCX format.',
        convertBatch: convertBatch
    };
}