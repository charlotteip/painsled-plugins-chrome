/*
 This file is from the painsled-plugins repository, https://github.com/charlotte-ip/painsled-plugins
 Copyright (c) 2015, Charlotte Intellectual Properties, LLC, All rights reserved.

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 3.0 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library.
 */

// This plugin dumps workout and stroke data as JSON text.
function PainsledPluginLibrary() {
    // Batch conversion API
    function convertWorkoutBatch(workouts) {
        // Temporary hack for PainSled <= v0.8 which passes workouts in latest-first order.
        workouts.sort(function(a, b) {return a.startMs - b.startMs});

        return {content: '{"workouts": ' + JSON.stringify(workouts, null, 2) + '\n}', error: false};
    }

    // Return public members
    return {
        mimeType: 'application/json',
        fileExt: 'json',
        shortName: 'json',
        description: 'Convert PainSled data to JSON.',
        convertBatch:  convertWorkoutBatch
    };
}